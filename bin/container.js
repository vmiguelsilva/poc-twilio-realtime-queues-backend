'use strict';

const { asClass, asValue, asFunction, createContainer } = require('awilix');

const Express = require('../src/infra/express');
const registerRoutes = require('../src/api');
const webHookRoutes = require('../src/api/webHook');
const queueRoutes = require('../src/api/queues');
const logger = require('../src/infra/logger');
const handlers = require('../src/middlewares/handlers');
const config = require('../src/config');
const SocketService = require('../src/service/socket');
const twilioClient = require('../src/common/twilioClient');
const TaskRouterService = require('../src/service/taskRouter');

/**
 * Registering all dependencies in a container
 * @param {Object} deps extra dependencies 
 */
function registerContainer(deps = null) {
  const container = createContainer();
  
  container.register({
    // config
    port: asValue(config.HTTP_PORT),
    twilioVariables: asValue({
      accountSid: config.TWILIO_ACCOUNT_SID,
      authToken: config.TWILIO_AUTH_TOKEN,
      workSpaceSid: config.TWILIO_FLEX_WORKSPACE_SID
    }),

    // services
    socketService: asClass(SocketService).classic().singleton().disposer((skt) => skt.dispose()),
    taskRouterService: asClass(TaskRouterService).classic().singleton(),

    // infra
    logger: asValue(logger),
    express: asClass(Express).classic().singleton().disposer((express) => express.dispose()),

    // routes handlers and middlewares
    registerRoutes: asFunction(registerRoutes).singleton().proxy(),
    webHookRoutes: asFunction(webHookRoutes).singleton().proxy(),
    queueRoutes: asFunction(queueRoutes).singleton().proxy(),
    handlers: asFunction(handlers).singleton().proxy(),

    // common 
    twilioClient: asFunction(twilioClient).singleton().proxy(),

    // to override dependencies
    ...deps
  });

  return container;
}

module.exports = registerContainer;
