'use strict';

const VError = require('verror');
const blocked = require('blocked')

const registerContainer = require('./container');
const logger = require('../src/infra/logger');
const awilix = require('awilix');
awilix.createContainer()

/**
 * Init application.
 * Attach shutdown events.
 * Attach blocker listener.
 * @param {Object} deps To override dependencies 
 */
async function init(deps = null) {
  try {
    logger.info('Service starting...');
    const container = registerContainer(deps);

    attachShutdown(container);
    attachBlocker();

    await initServer(container);

    logger.info('Service started.');

    return container;
  } catch (error) {
    logger.error(error, 'Failed to initialize webHook service.');
    process.kill(process.pid, 'SIGINT');
  }
}


/**
 * Initialize HTTP server
 * @param {AwilixContainer} container Awilix container instance
 */
async function initServer(container) {
  try {
    const server = container.resolve('express');
    server.on('info', (info) => logger.info(info));
    server.on('error', (err) => {
      logger.error(new VError(err, 'Express server emitted an error'));
      process.kill(process.pid, 'SIGINT');
    });

    await server.init();
  } catch (err) {
    console.log(err)
    throw new VError(err, 'Failed to initialize the Express server');
  }
}


/**
 * Graceful shutdown
 * @param {AwilixContainer} container Awilix container instance
 */
async function attachShutdown(container) {
  /**
   * Dispose container
   */
  async function dispose() {
    logger.info('Disposing the webHook...');

    try {
      await container.dispose();

      logger.info('Disposed the webHook');
      process.exit(0);
    } catch (error) {
      logger.error(new VError(error, 'Failed to shutdown application'));
      process.exit(1);
    } finally {
      logger.dispose();
    }
  }

  process.on('SIGHUP', dispose);
  process.on('SIGINT', dispose);
  process.on('SIGTERM', dispose);
}

/**
 * Attach blocker listener.
 */
function attachBlocker() {
  blocked((ms) => {
    logger.warn({ blockedInMs: ms }, `The event loop was blocked for ${ms}ms`);
  }, { threshold: 10 });
}

if (process.env.NODE_ENV !== 'test') {
  init();
}
