'use strict';

const http = require('http');
const EventEmitter = require('events');

/**
 * @class HttpServer
 * @extends {EventEmitter}
 */
class HttpServer extends EventEmitter {
  /**
   * Return a new HttpServer instance
   * @param {Server} app 
   * @param {Number} port 
   */
  constructor(app, port) {
    super();
    this.app = http.createServer(app);
    this.port = port;
  }

  /**
   * @returns {Promise} Initialize server listener
   * @memberof HttpServer
   */
  init() {
    return new Promise((resolve, reject) => {
      this.app.on('error', (err) => {
        if (err.syscall !== 'listen') {
          this.emit('error', new Error(`Unexpected error on http server: ${err.message}`));
          return;
        }
        const bind = typeof this.port === 'string'
          ? 'Pipe ' + this.port
          : 'Port ' + this.port;

        switch (err.code) {
          case 'EACCES':
            return reject(new Error(`${bind} requires elevated privileges: ${err.message}`));
          case 'EADDRINUSE':
            return reject(new Error(`${bind} is already in use: ${err.message}`));
          default:
            return reject(new Error(`Unexpected error on http server: ${err.message}`));
        }
      });
      this.app.listen(this.port, () => {
        this.emit('info', `Server is listening on ${this.port}`);
        resolve(this.app);
      });
    });
  }


  /**
   * @param {Function} cb
   * @memberof HttpServer
   */
  stop(cb) {
    this.app.close(cb);
  }

  /**
   * @memberof HttpServer
   */
  dispose() {
    return new Promise((resolve, reject) => {
      this.app.close((err) => {
        if (err) return reject(err);
        resolve();
      });
    });
  }
}

module.exports = HttpServer;
