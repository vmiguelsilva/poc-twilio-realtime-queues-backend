'use strict';

const EventEmitter = require('events');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');

const HttpServer = require('./http-server');

/**
 * Setup http server
 */
class Express extends EventEmitter {
  /**
   * Creates an instance of Express.
   *
   * @param {String} port
   * @param {Function} registerRoutes
   * @param {Object} handlers
   * @param {Function} socketService
   * @memberof Express
   */
  constructor(port, registerRoutes, handlers, socketService) {
    super();

    this.app = express();
    this.port = port;
    this.registerRoutes = registerRoutes;
    this.handlers = handlers;
    this.io = socketService;
  }

  /**
   * Initialize http server with routes and middlewares
   */
  async init() {
    try {
      const routes = this.registerRoutes;

      // Enable cross-origin requests
      this.app.use(cors('*'));

      // Remove unnecessary exposed headers
      this.app.use(helmet());

      // Allow json parser on body
      this.app.use(bodyParser.urlencoded({ extended: true }));
      this.app.use(bodyParser.json());

      this.app.use(this.handlers.logging);
      this.app.use('/', routes);
      this.app.use(this.handlers.error);

      const server = await this.registerHttpServer();
      this.io.initSocket(server);
    } catch (err) {
      throw new Error(`App: Failed to start the http-server ${err.message}\n${err.stack}`);
    }
  }

  /**
   * Gracefull shutdown
   */
  dispose() {
    return new Promise((resolve, reject) => {
      if (!this.httpServer || !this.httpServer.stop) {
        return resolve();
      }
      this.httpServer.stop((err) => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
    });
  }

  /**
   * Initialize http server
   */
  registerHttpServer() {
    this.httpServer = new HttpServer(this.app, this.port);
    this.httpServer.on('error', (err) => {
      this.emit('error', err);
    });
    this.httpServer.on('info', (msg) => {
      this.emit('info', msg);
    });

    return this.httpServer.init();
  }
}

module.exports = Express;
