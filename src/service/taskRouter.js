const VError = require('verror');
const moment = require('moment');

/**
 * @class TaskRouterService
 */
class TaskRouterService {
  /**
   * Return a new HttpServer instance
   * @param {Logger} logger beautiful logger 
   * @param {Twilio} twilioClient TwilioClient instance
   * @param {twilioVariables}  twilioVariables Envirounment variables refer twilio
   */
  constructor(logger, twilioClient, twilioVariables) {
    this.logger = logger;
    this.workSpaceSid = twilioVariables.workSpaceSid;
    this.twilioClient = twilioClient;
  }

  /**
   * Returns all TaskQueues with respective statistics
   * 
   * @memberof TaskRouterService
   * 
   * @returns {Promise} All TaskQueues by workspace configurated
   */
  async getAllQueues() {
    try {
      const queuePromises = [];
      const queues = await this.twilioClient
        .taskrouter
        .workspaces(this.workSpaceSid)
        .taskQueues.list();

      queues.forEach(queue => {
        queuePromises.push(
          this.getStatsByQueue(queue)
        )
      });

      return Promise.all(queuePromises);
    } catch (err) {
      console.error(err);
      return new VError({msg: err.message}, err);
    }

  }

  /**
   * Collect all statistics by queue
   * 
   * @memberof TaskRouterService
   * 
   * @param {Queue} queue Queue with simple information
   * @param {String} channel 
   * 
   * @returns {Queue} Queue with statistics
   */
  async getStatsByQueue(queue, channel = 'voice') {
    try {
      const queueStatsCumulativeStats = await this.twilioClient
        .taskrouter
        .workspaces(this.workSpaceSid)
        .taskQueues(queue.sid)
        .statistics()
        .fetch({ 
          taskChannel: channel,
          startDate: moment().startOf('day').toDate(),
          endDate: moment().endOf('day').toDate()
         })

      const cumulativeStats = this.minimizeCumulativeStats(queueStatsCumulativeStats.cumulative)
      const realTimeStats = this.minimizeRealTimeStats(queueStatsCumulativeStats.realtime)
      
      queue = Object.assign(queue, {
        ...realTimeStats,
        ...cumulativeStats
      })

      return this.getWorkersByQueue(queue);
      
    } catch (error) {
      this.logger.error(new VError(error));
      return queue;
    }
  }

  /**
   * Collect workers information of Twilio API by TaskQueue expression
   * 
   * @memberof TaskRouterService
   * 
   * @param {Queue} queue Queue with simple information
   * 
   * @returns {Promise} Return workers information per TaskQueue expression
   */
  async getWorkersByQueue(queue) {
    queue.workers = [];
    try {
      const workers = await this.twilioClient
        .taskrouter
        .workspaces(this.workSpaceSid)
        .workers
        .list({
          targetWorkersExpression: queue.targetWorkers
        });

      for (const [index, worker] of workers.entries()) {
        const attr = JSON.parse(worker.attributes);
        queue.workers[index] = { 
          name: attr.full_name,
          status: worker.activityName,
          sid: worker.sid,
          dateStatusChanged: worker.dateStatusChanged,
        }
      }
      return queue;
    } catch (error) {
      return queue;
    }
  }

  /**
   * Reduce cumulative statistics
   * 
   * @memberof TaskRouterService
   * 
   * @param {QueueCumulativeStatistics} cumulativeStats 
   * 
   * @returns {Object} Cumulative statistics reduced
   */
  minimizeCumulativeStats(cumulativeStats = null) {
    if (!cumulativeStats) {
      return cumulativeStats;
    }
    return {
      averageWaitTime: cumulativeStats.wait_duration_until_accepted.avg,
      averageAcceptanceTime: cumulativeStats.avg_task_acceptance_time,
      tasksAssigned: cumulativeStats.tasks_assigned,
      tasksCompleted: cumulativeStats.tasks_completed,
      tasksPending: cumulativeStats.tasks_pending,
      tasksCanceled: cumulativeStats.tasks_canceled
    }
  }

  /**
   * Reduce realtime statistics
   * 
   * @memberof TaskRouterService
   * 
   * @param {QueueRealtimeStatistics} realTimeStats 
   * 
   * @returns {Object} Realtime statistics reduced
   */
  minimizeRealTimeStats(realTimeStats = null) {
    if (!realTimeStats) {
      return realTimeStats;
    }
    return {
      totalAvailableWorkers: realTimeStats.total_available_workers,
      totalEligibleWorkers: realTimeStats.total_eligible_workers
    }
  }

}

module.exports = TaskRouterService;
