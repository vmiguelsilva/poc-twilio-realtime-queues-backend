'use strict';

const socketIO = require('socket.io')

/**
 * @class Socket
 */
class Socket {
  constructor(logger) {
    this.logger = logger;
  }

   /**
   * Attach socket and notification events and start the socket server.
   * @param server The HTTP server that we're going to bind to 
   *
   * @memberof Socket
   * @returns {null} void return 
   */
  initSocket(server) {
    this.io = socketIO(server);
    this.attachEvents();
  }

  /**
   * Dispose socket server.
   *
   * @returns {Promise} Promise of close socket connection
   * @memberof Socket
   */
  dispose() {
    return new Promise((resolve) => {
      this.io.close(resolve);
    });
  }

  /**
   * Attach notification events.
   *
   * @memberof Socket
   */
  attachEvents() {
    // Attach socket connection event.
    this.io.on('connection', (socket) => {
      // TODO: emit events with all informations by queues and workers
    });

    // Attach error events.
    this.io.on('error', (e) => this.logger.error(e));

  }
}

module.exports = Socket;
