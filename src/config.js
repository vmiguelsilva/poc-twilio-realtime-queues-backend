'use strict';
require('dotenv').config(process.env.NODE_ENV === 'test' && { path: '.env.test' });

const Joi = require('joi');

const configSchema = Joi.object({
  HTTP_PORT: Joi.number().required(),
  TWILIO_ACCOUNT_SID: Joi.string().required(),
  TWILIO_AUTH_TOKEN: Joi.string().required(),
  TWILIO_FLEX_WORKSPACE_SID: Joi.string().required()
});

const config = {
  HTTP_PORT: process.env.HTTP_PORT,
  TWILIO_ACCOUNT_SID: process.env.TWILIO_ACCOUNT_SID,
  TWILIO_AUTH_TOKEN: process.env.TWILIO_AUTH_TOKEN,
  TWILIO_FLEX_WORKSPACE_SID: process.env.TWILIO_FLEX_WORKSPACE_SID
};

const result = configSchema.validate(config);
if (result.error) {
  console.error(result.error);
  process.exit(1);
}

module.exports = result.value;
