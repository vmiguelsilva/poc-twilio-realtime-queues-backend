const twilio = require('twilio');


/**
 * 
 * @returns {Twilio} Return a new TwilioClient instance 
 * @param {Object} injectedDependencies 
 */
function twilioClient({twilioVariables}) {
  return twilio(
    twilioVariables.accountSid,
    twilioVariables.authToken
  )
}

module.exports = twilioClient;
