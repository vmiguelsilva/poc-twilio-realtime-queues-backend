'use strict';

const VError = require('verror');

const { getCommonRequestDetails } = require('../utils');

/**
 * Handler for errors and logging
 * @param {Object} injectedDependencies 
 */
function handler({ logger }) {
  return {
    error: (err, req, res, next) => {
      if (!err.status || err.status >= 500 && err.status < 600) {
        logger.error(new VError({
          cause: err,
          info: getCommonRequestDetails(req)
        }, `Unexpected error on request to ${req.method} ${req.url}`));
      }

      res.status(err.status || 500).send({ error: err.message || err });
    },
    logging: (req, res, next) => {
      logger.info(getCommonRequestDetails(req), `Requested the endpoint: ${req.method} ${req.url}`);

      next();
    },
  };
}

module.exports = handler;
