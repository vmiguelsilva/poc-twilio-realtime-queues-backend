'use strict';

/**
 * Get some info abount the request being made
 * @param {*} req 
 */
const getCommonRequestDetails = (req) => ({
  method: req.method,
  status: req.status,
  headers: req.headers,
  url: req.url,
  ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
  body: req.body,
  params: req.params,
  query: req.query
});

module.exports = {
  getCommonRequestDetails
}
