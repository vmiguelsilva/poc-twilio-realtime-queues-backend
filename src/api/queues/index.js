'use strict';

const VError = require('VError');
const { Router } = require('express');


/**
 * 
 * These routes have the responsibility of returning all TaskQueue resources. 
 * This includes workers and statistics
 * 
 * @param {Object} injectedDependencies 
 * @returns {Router} Routes of queue resource
 */
function queueRegisterRouter({ taskRouterService, logger }) {

  const router = new Router();

  router.get('/queue', async (req, res) => {
    try {
      const queues = await taskRouterService.getAllQueues();
      res.send(queues).status(200);
    } catch (error) {
      console.log(error)
      logger.error(new VError(error));
      res.status(500).send(error.message);
    }
  });

  return router;
}

module.exports = queueRegisterRouter;
