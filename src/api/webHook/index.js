'use strict';

const { Router } = require('express');

/**
 * 
 * webHookRoute handles all the incoming TaskRouter's events.
 *
 * The TaskRouter events callback should be configurated to the endpoint
 * that use this function as his handler. The handler will listen to all
 * the TaskRouter's events.
 *
 * @param {Object} injectedDependencies 
 * @returns {Router} Routes of webhook resource
 */
function webHookRegisterRouter({ socketService, taskRouterService }) {
  const router = new Router();

  router.post('/webhook', (req, res) => {
    try {
      if (/worker.activity/.test(req.body.EventType)) {
        taskRouterService.getAllQueues().then(queues => {
          socketService.io.emit('worker-go', queues)
        }).catch(console.log)
      }
        taskRouterService.getAllQueues().then(queues => {
          socketService.io.emit('queue-go', queues)
        }).catch(console.log)
      res.send('ok').status(204);
      
    } catch (error) {
      console.log(error);
      res.status(204);
    }
  });

  return router;
}

module.exports = webHookRegisterRouter;
