'use strict';

const { Router } = require('express');

/**
 * Setup routes
 * @param {Object} injectedDependencies 
 */
function registerRoutes({ webHookRoutes, queueRoutes }) {
    const router = new Router();
    router.use(webHookRoutes);
    router.use(queueRoutes);

    return router;
}

module.exports = registerRoutes;
