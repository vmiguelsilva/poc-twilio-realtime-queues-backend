# [POC] Twilio real-time queues dashboard

This repo performs all events received by Twilio and gets information about [TaskQueues](https://www.twilio.com/docs/flex/routing/api/task-queue) and [Workers](https://www.twilio.com/docs/flex/routing/api/worker).

You will need a [Twilio account](https://www.twilio.com/try-twilio) and an open domain to execute it.

For local tests, you use [Ngrok](https://ngrok.com/) to open your local HTTP server.

# Install

Run `yarn install` to install all dependencies

# Setup

Create `.env` on root path with based on `.env.example` and fill in the environment variables.

# Execute

Run `yarn start`

If you're in a local environment, don't forget start [Ngrok](https://ngrok.com/) with the port you configure in `.env` file


For more information access [documentation page](https://vmiguelsilva.bitbucket.io/real-time-queues/)
